class LogExtractor:
    """Extracts all matching log sections starting with an end tag and ending with an end tag"""

    def __init__(self, start_tag: str, end_tag: str, contained_tag: str = None) -> None:
        """Constructor

        :param start_tag:   Start tag to search for. This marks the beginning of all matching sections
        :param end_tag:     End tag to search for. This marks the end of all matching sections
        """
        self._start_tag = start_tag
        self._end_tag = end_tag
        self._contained_tag = contained_tag

    def extract_matching_sections_from_file(self, filename: str) -> list[str]:
        """Extracts all matching sections from a file
        
        :param filename:    Name of a file to search
        :returns:           List of strings of matching sections in the search file
        """
        capture = False
        sections = []
        output = ''
        with open(filename, 'rt') as input:
            for line in input.readlines():
                if self._start_tag in line:
                    capture = True
                if capture and len(line.strip()) > 0:
                    output += line
                if self._end_tag in line:
                    capture = False
                    if len(output) > 0:
                        if not self._contained_tag or self._contained_tag in output:
                            sections.append(output)
                    output = ''
        return sections

    def extract_matching_sections(self, filenames: list[str]) -> dict[str, list[str]]:
        """Extracts all matching sections from a list of files
        
        :param filenames:   List of file names to search
        :returns:           A map of all matches indexed by file name. Each map value will be a list of strings of matching sections in that file
        """
        sections = {}
        for filename in filenames:
            sections[filename] = self.extract_matching_sections_from_file(filename)
        return sections
