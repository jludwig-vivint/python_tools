"""Library of log related utilities"""
import logging
import sys

SUCCESS_LEVEL_NUM = 25  # Choose a level number appropriately

def enable_pretty_logging(logger):
    logging.addLevelName(SUCCESS_LEVEL_NUM, "SUCCESS")

    def success(self, message, *args, **kws):
        # if self.isEnabledFor(SUCCESS_LEVEL_NUM):
        self._log(SUCCESS_LEVEL_NUM, message, args, **kws) 

    logging.Logger.success = success

    class CustomFormatter(logging.Formatter):
        """Logging Formatter to add colors"""

        grey = "\x1b[38;21m"
        blue = "\x1b[34m"
        green = "\x1b[32m"
        light_green = "\x1b[32m[92m"
        yellow = "\x1b[33m"
        red = "\x1b[31m"
        bold_red = "\x1b[1;31m"
        reset = "\x1b[0m"
        format_start = "%(asctime)s "
        format_end = "%(levelname)-8s" + reset + " %(message)s"

        FORMATS = {
            logging.DEBUG: format_start + blue + format_end,
            logging.INFO: format_start + green + format_end,
            SUCCESS_LEVEL_NUM: format_start + light_green + format_end,
            logging.WARNING: format_start + yellow + format_end,
            logging.ERROR: format_start + red + format_end,
            logging.CRITICAL: format_start + bold_red + format_end
        }

        def format(self, record):
            log_fmt = self.FORMATS.get(record.levelno)
            formatter = logging.Formatter(log_fmt)
            return formatter.format(record)

    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(CustomFormatter())
    logger.addHandler(handler)
