import logging

from ssh_host import SshHost

logger = logging.getLogger()

class Camera(SshHost):
    def __init__(self, host_name, alias=None) -> None:
        self.host_name = host_name
        self.name = alias if alias else host_name
        super().__init__(host_name, 'root')

    def mount_rw(self):
        self.command(['mount', '-o', 'rw,remount', '/'])

    def mount_ro(self):
        self.command(['mount', '-o', 'ro,remount', '/'])

    def sync(self):
        self.command(['sync'])

    def reboot(self):
        self.command(['reboot'])

    def update_fw(self, image_filename):
        logger.info(f'Uploading FW to camera {self.name}...')
        self.upload_fw(image_filename)
        logger.info(f'Updating FW on camera {self.name}...')
        self.command(['/usr/local/bin/tek_ota', '--flash', '/mnt/media/upgrade.7z'])
        logger.info(f'Rebooting camera {self.name}...')
        self.reboot()

    def upload_fw(self, image_filename):
        self.upload_files(image_filename, '/mnt/media/upgrade.7z')
