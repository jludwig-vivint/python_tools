import asyncio
import logging
import time

from .camera import Camera

from typing import Callable

_logger = logging.getLogger()

def for_each_camera(cameras: list[Camera], func: Callable, task_name: str, base_retry_seconds: float=5):
    retries = 5
    cameras_left = cameras
    while len(cameras_left) > 0 and retries > 0:
        failed_cameras = []
        for camera in cameras_left:
            _logger.debug(f'Running task "{task_name}" on camera {camera.name}...')
            if not func(camera):
                _logger.error(f'Failed to run task "{task_name}" on camera {camera.name}')
                failed_cameras.append(camera)
            else:
                _logger.debug(f'Task "{task_name}" succeeded on camera {camera.name}')
        cameras_left = failed_cameras
        if len(cameras_left) > 0:
            _logger.error(f'Failed to run task "{task_name}" on {len(cameras_left)} cameras')
            retries -= 1
            if retries > 0: 
                _logger.warn(f'Retrying task "{task_name}" in {base_retry_seconds} seconds...')
                time.sleep(base_retry_seconds)
                base_retry_seconds *= 2

async def for_each_camera_async(cameras: list[Camera], async_func: Callable, task_name: str, base_retry_seconds: float=5):
    retries = 5
    cameras_left = cameras
    while len(cameras_left) > 0 and retries > 0:
        tasks = []
        for camera in cameras_left:
            _logger.debug(f'Running task "{task_name}" on camera {camera.name}...')
            tasks.append(async_func(camera))
        results = await asyncio.gather(*tasks, return_exceptions=True)
        failed_cameras = []
        for i, result in enumerate(results):
            camera = cameras_left[i]
            if isinstance(result, Exception):
                _logger.error(f'Exception running task "{task_name}" on camera {cameras.name}: {result}')
                failed_cameras.append(camera)
            elif result:
                _logger.debug(f'Task "{task_name}" succeeded on camera {camera.name}')
            else:
                _logger.error(f'Failed to run task "{task_name}" on camera {camera.name}')
                failed_cameras.append(camera)

        cameras_left = failed_cameras
        if len(cameras_left) > 0:
            _logger.error(f'Failed to run task "{task_name}" on {len(cameras_left)} cameras')
            retries -= 1
            if retries > 0: 
                _logger.warn(f'Retrying task "{task_name}" in {base_retry_seconds} seconds...')
                time.sleep(base_retry_seconds)
                base_retry_seconds *= 2
