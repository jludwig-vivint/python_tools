from camera import Camera

NELSONS_REBOOT_TEST_CAMERAS = {
    'cam1': '10.1.68.232', # Camera 1
    # 'cam2': '10.1.68.238', # Camera 2
    'cam3': '10.1.68.243', # Camera 3
    'cam4': '10.1.68.233', # Camera 4
    'cam5': '10.1.68.239', # Camera 5
    'cam6': '10.1.68.231', # Camera 6
    'cam7': '10.1.68.237', # Camera 7
    'cam8': '10.1.68.234', # Camera 8
    'cam9': '10.1.68.236', # Camera 9
}

CAMERA_GROUPS = {
# Group names must be lower case
    'nelson': NELSONS_REBOOT_TEST_CAMERAS,
}

def parse_camera_list(camera_names: list[str]) -> list[Camera]:
    cameras = []
    for camera_name in camera_names:
        if camera_name.lower() in CAMERA_GROUPS.keys():
            camera_group = CAMERA_GROUPS[camera_name]
            cameras += [Camera(camera_host_name, camera_name) for camera_name, camera_host_name in camera_group.items()]
        else:
            cameras.append(Camera(camera_name))
    return cameras