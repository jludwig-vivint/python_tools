import os
import subprocess

class SshHost:
    def __init__(self, host_name, user_name) -> None:
        self.host = f'{user_name}@{host_name}'

    def command(self, cmd_args, system=False):
        if system:
            args = ['ssh', '-o', 'ConnectTimeout=5', self.host] + cmd_args
            command = ' '.join([ f'"{arg}"' if len(arg.split()) > 1 else arg for arg in args ])
            os.system(command)
            return ''.encode('utf-8')
        return subprocess.check_output(['ssh', '-o', 'ConnectTimeout=5', self.host] + cmd_args)

    def upload_files(self, source, destination):
        subprocess.check_call(['scp', '-o', 'ConnectTimeout=5', source, f'{self.host}:{destination}'])

    def download_files(self, source, destination):
        subprocess.check_call(['scp', '-o', 'ConnectTimeout=5', f'{self.host}:{source}', destination])
