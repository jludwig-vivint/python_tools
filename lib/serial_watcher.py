import logging
import serial
import threading
import time
from time_limited_queue import TimeLimitedQueue

_OUTPUT_FILE_TIMESTMAP_FORMAT = r'%Y-%m-%d_%H-%M-%S'
CONTEXT_TAG = r'{ctx}'
DEVICE_NAME_TAG=r'{device}'
TIMESTAMP_TAG = r'{ts}'
_OUTPUT_FILE_DEFAULT_PATTERN = f'serial_log_{TIMESTAMP_TAG}_{DEVICE_NAME_TAG}_{CONTEXT_TAG}.txt'

_logger = logging.getLogger()

DEFAULT_WATCH_TIMEOUT_SECONDS=300
DEWAULT_WATCH_CONTEXT_STRING='watch_event_triggered'
class _Watch:
    def __init__(self, search_string, context_string, timeout_seconds) -> None:
        self.search_string = search_string
        self.context_string = context_string
        self.timeout_seconds = timeout_seconds
        self.expiration = None

def parse_watch_string(watch_string: str) -> object:
    """Parses a watch string

    Watch string format: [[<timeout_seconds>]:<context_string>]:<search_string>

    :param watch_string:    The watch string to parse

    :returns:   An objects with fields set to the parsed values
    """
    parts = watch_string.split(':')
    if len(parts) > 3:
        parts = parts[0:2] + [':'.join(parts[2:])]
    timeout_seconds = float(parts[-3]) if len(parts) > 2 else DEFAULT_WATCH_TIMEOUT_SECONDS
    context_string = parts[-2] if len(parts) > 1 else DEWAULT_WATCH_CONTEXT_STRING
    return _Watch(
        search_string=parts[-1],
        context_string=context_string,
        timeout_seconds=timeout_seconds
    )

class SerialWatcher:
    def __init__(
            self,
            device_name:str,
            port_name: str,
            baud_rate: int = 115200,
            output_filename_pattern=_OUTPUT_FILE_DEFAULT_PATTERN
        ) -> None:
        self._validate_filename_pattern(output_filename_pattern)
        self._device_name = device_name
        self._port_name = port_name
        self._baud_rate = baud_rate
        self._run_thread = True
        self._thread = threading.Thread(target=self._run)
        self._queue = TimeLimitedQueue(300, 500)
        self._output_filename_pattern = output_filename_pattern
        self._inactivity_timeout_seconds = None
        self._inactivity_handled = False
        self._watch = None

    def start(self) -> None:
        self._run_thread = True
        self._thread.start()

    def stop(self) -> None:
        _logger.info('Stopping serial watcher...')
        self._run_thread = False
        self._thread.join()

    @property
    def running(self):
        return self._run_thread

    def reload_config(self) -> None:
        """TODO: This function will see if the serial port needs to be watched and start and stop the watcher as necesssary"""
        _logger.info('Reloading serial watcher config...')

    def set_inactivity_timeout(self, timeout_seconds: float) -> None:
        """Sets the optional inactivity timeout and pattern

        When no serial output has been received in the specified timeout, all serial will output
        stored in the queue will be written to a file. The name of the file will be controlled by
        inactivity_filename_pattern, which may contain a directory at the beginning of the pattern

        :note:  inactivity_filename_pattern must contain the time stamp tag '{ts}', the context tag '{ctx}' and the device name tag '{device}'
        
        :param timeout_seconds: The inactivity timeout in seconds
        """
        self._inactivity_timeout_seconds = timeout_seconds
        _logger.info(f'Set inactivity timeout to {self._inactivity_timeout_seconds} seconds')

    def dump_output_to_file(self, context_string: str='user_requested', truncate: bool=True):
        """Dumps all output data to a file using a file name pattern, optionally clearing the queue

        :note:  filename_pattern must contain the time stamp tag '{ts}', the context tag '{ctx}' and the device name tag '{device}'

        :param context:     Context for the output file name
        :param truncate:    Discard all output written to the ouptut file from the queue
        """
        self._write_ouptput_to_file(context=context_string, truncate=truncate)

    def set_watch(self, watch: _Watch) -> None:
        self._watch = watch
        _logger.info(f'Set watch string to "{self._watch.search_string}" with context "{self._watch.context_string}" amd {self._watch.timeout_seconds} second timeout')

    def _run(self) -> None:
        """Reads lines from the serial port and adds them to the queue

        Also checks for inactivity on the serial port and dumps collected serial output
        to an output file using the inactivity filename pattern
        """
        _logger.info('Serial watcher started')
        # Open the serial port
        with serial.Serial(self._port_name, self._baud_rate, timeout=1) as port:
            while self._run_thread:
                line = port.readline().decode('utf-8', errors='ignore')
                if line:
                    print(line.strip())
                    self._store_line(line)
                else:
                    self._check_for_inactivity()
                self._handle_watch()
        _logger.info('Serial watcher exiting')
    
    def _store_line(self, line: str) -> None:
        """Stores a line read from the serial port to the queue"""
        self._queue.append(line)
        # We need to restart checking for inactivity
        # The age of the new entry will be the criteria for an inactivty timeout
        self._inactivity_handled = False
        _logger.debug('Activity detected: item added')
        self._check_for_watch(line)

    def _check_for_inactivity(self) -> None:
        """Checks for and handles inactivity, if an inactivity timeout is set"""
        if self._inactivity_timeout_seconds:
            _logger.debug('Checking for new inactivity...')
            if self._inactivity_handled:
                _logger.debug('Aborting check, because there has been no activity since inactivity was last handled')
                return
            # Wait for the age of the last entry added to the queue to exceed the inactivty timeout
            _logger.debug('Checking for inactivity...')
            if len(self._queue) > 0:
                _logger.debug(f'Most recent serial output is {self._queue[-1].age} seconds old')
            if len(self._queue) > 0 and self._queue[-1].age > self._inactivity_timeout_seconds:
                try:
                    _logger.info(f'Inactivity longer than {self._inactivity_timeout_seconds} seconds detected. Dumping collected serial output to file')
                    # TODO: Is not truncating for inactivity and truncating for user requested files
                    # the right behavior? I think it is. I also need to handle automatically logging
                    # reboots. I think this is the right policy:
                    # User requested: user choice, default to truncate
                    # Reboot detected: truncate
                    # Inactivity detected: don't truncate
                    self._write_ouptput_to_file(context='inactivity_detected', truncate=False)
                    # To prevent repetitively detecting a time out
                    self._inactivity_handled = True
                    _logger.debug('Inactivity handled')
                except Exception as e:
                    logging.warn(f'SerialWatcher: Exception while handling inactivity event: {e}')

    def _check_for_watch(self, line: str) -> None:
        if not self._watch:
            return
        if self._watch.search_string in line:
            _logger.info(f'Found watch string "{self._watch.search_string}" for event "{self._watch.context_string}')
            # Found the watch string:
            # 1. Extend the max queue's max age by self._watch.timeout_seconds
            #    This will cover everything we already have and include everything that happens until the watch expiration
            self._queue.extend_expiration_time(self._watch.timeout_seconds)
            # 2. Set an exparationtime self._watch.timeout_seconds in the future at wich point the watch will trigger an output file dump
            self._watch.expiration = time.time() + self._watch.timeout_seconds

    def _handle_watch(self) -> None:
        if self._watch and self._watch.expiration:
            _logger.debug('Checking for watch expiration')
            if time.time() > self._watch.expiration:
                _logger.info(f'Timaout after Watch string "{self._watch.search_string}" has expired')
                try:
                    self._write_ouptput_to_file(context=self._watch.context_string, truncate=True)
                    self._queue.reset_expiration_time_extension()
                    self._watch.expiration = None
                    _logger.debug("Set watch handled")
                except Exception as e:
                    _logger.error(f'SerialWatcher: Exception while handling watch event: {e}')

    def _write_ouptput_to_file(self, context: str, truncate: bool) -> None:
        """Writes collected serial output to a file
        
        :param context:     Context string for output file name
        :param truncate:    Discard all output written to the ouptut file from the queue
        """
        filename = self._generate_filename(context)
        _logger.info(f'Dumping collected serial output to "{filename}"...')
        with open(filename, 'wt') as output:
            if truncate:
                data = self._queue.pop_all()
                self._inactivity_handled = False
                _logger.debug('Activity detected: queue cleared')
            else:
                data = self._queue
            output.write(''.join(data))
        _logger.info(f'Dumped collected serial output to "{filename}"')

    def _validate_filename_pattern(self, filename_pattern: str) -> None:
        """Validates a filename pattern string
        
        :note:  The string must contain the time stamp tag '{ts}', the context tag '{ctx}' and the device name tag '{device}'

        :param filename_patter: The filename pattern string to validate
        """
        if CONTEXT_TAG not in filename_pattern:
            raise Exception(f'No required "{CONTEXT_TAG}" in ouptut file pattern string "{filename_pattern}"')
        if DEVICE_NAME_TAG not in filename_pattern:
            raise Exception(f'No required "{DEVICE_NAME_TAG}" in ouptut file pattern string "{filename_pattern}"')
        if TIMESTAMP_TAG not in filename_pattern:
            raise Exception(f'No required "{TIMESTAMP_TAG}" in ouptut file pattern string "{filename_pattern}"')

    def _generate_filename(self, context: str) -> None:
        """Genrates a file name based on the filename patter
        
        :note:  The string must contain the time stamp tag '{ts}', the context tag '{ctx}' and the device name tag '{device}'

        :param context: Context for the ouput file name
        """
        time_stamp = time.strftime(_OUTPUT_FILE_TIMESTMAP_FORMAT, time.localtime(time.time()))
        return self._output_filename_pattern.replace(TIMESTAMP_TAG, time_stamp).replace(DEVICE_NAME_TAG, self._device_name).replace(CONTEXT_TAG, context)
