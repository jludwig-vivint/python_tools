from collections import deque
import threading
import time
from typing import Any

class _QueueEntry:
    def __init__(self, item, timestamp) -> None:
        self.item = item
        self.timestamp = timestamp
    
    @property
    def age(self):
        return time.time() - self.timestamp

class TimeLimitedQueue:
    """Queue that stores only items with a certain max age in seconds once the queue exceeds its minimum size
    
    :note:  Items older than the max age will only be purged once the queue has hit its minimum size and then
            only items beyond the minimum size of the queue will be aged out
    :note:  Pruning is only applied when an item is pushed to the queue
    """

    def __init__(self, max_age_seconds: float, min_depth: int) -> None:
        """Constructor
        
        :parama max_age_seconds:    The maximum age of items beyound the minimum size of the queue that will be stored
        :param min_depth:           The minimum size of the queue that must be met before expired items are aged out
        """
        self._max_age_seconds_base = max_age_seconds
        self._time_extension_seconds = 0
        self._min_depth = min_depth
        self._queue = deque()
        self._lock = threading.Lock()

    def extend_expiration_time(self, time_seconds):
        self._time_extension_seconds += time_seconds

    def reset_expiration_time_extension(self):
        if self._time_extension_seconds != 0:
            self._time_extension_seconds = 0
            self._prune()
    
    def append(self, item: Any) -> None:
        """Add an item to the deque
        
        :param item:    Item to add
        """
        with self._lock:
            self._queue.append(_QueueEntry(item=item, timestamp=time.time()))
            self._prune()
    
    def pop_all(self):
        """Removes all entries from the queue and returns them as a list"""
        with self._lock:
            data = [entry.item for entry in self._queue]
            self._queue.clear()
            return data

    def __iter__(self) -> Any:
        with self._lock:
            for entry in self._queue:
                    yield entry.item

    def __len__(self):
        return len(self._queue)

    def __getitem__(self, index: int) -> Any:
        return self._queue[index]

    @property
    def _max_age_seconds(self):
        return self._max_age_seconds_base + self._time_extension_seconds

    def _prune(self) -> None:
        while len(self._queue) > self._min_depth and self._queue[0].age > self._max_age_seconds:
            self._queue.popleft()


